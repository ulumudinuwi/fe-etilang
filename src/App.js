import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Router, Switch, Route, Link, Redirect } from "react-router-dom";

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

import Login from "./components/Login";
import Register from "./components/Register";
import Home from "./components/Home";
import Profile from "./components/Profile";
import BoardModerator from "./components/BoardModerator";
import BoardAdmin from "./components/BoardAdmin";

import { logout } from "./actions/auth";
import { clearMessage } from "./actions/message";

import { history } from "./helpers/history";
import Navbar from "./components/Navbar";

// Users
import Users from "./components/User/ListUser";
import FormUsers from "./components/User/Form";

const App = () => {
  
  const hours = 1
  const saved = localStorage.getItem('saved')
  if (saved && (new Date().getTime() - saved > hours * 60 * 60 * 1000)) {
    localStorage.clear()
  }

  const [showModeratorBoard, setShowModeratorBoard] = useState(false);
  const [showAdminBoard, setShowAdminBoard] = useState(false);

  const { user: currentUser } = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  useEffect(() => {
    history.listen((location) => {
      dispatch(clearMessage()); // clear message when changing location
    });
  }, [dispatch]);

  useEffect(() => {
    if (currentUser) {
      setShowModeratorBoard(currentUser.role.includes("ppns"));
      setShowAdminBoard(currentUser.role.includes("admin"));
    }
  }, [currentUser]);

  const logOut = () => {
    dispatch(logout());
  };
  
  return (
    <Router history={history}>
      <div>
        
        {showAdminBoard && (
          <Route exact component={Navbar} />
        )}
        {showModeratorBoard && (
          <Route exact component={Navbar} />
        )}
        
        <div>
          <Switch>
            <Route exact path={["/", "/dashboard"]} component={Home} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/profile" component={Profile} />
            <Route path="/user/list" component={Users} />
            <Route path="/user/tambah" component={FormUsers} />
            <Route path="/mod" component={BoardModerator} />
            <Route path="/admin" component={BoardAdmin} />
          </Switch>
        </div>
      </div>
    </Router>
  );
};

export default App;
