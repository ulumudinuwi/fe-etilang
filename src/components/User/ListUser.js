import React, { useState, useEffect, useRef } from "react";
import UserService from "../../services/User/serviceUser";
import MUIDataTable from "mui-datatables";
import { useSelector } from "react-redux";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap";
import { Redirect } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUserEdit, faTrashAlt, faUnlockAlt, faEdit} from '@fortawesome/free-solid-svg-icons'
import {NotificationContainer, NotificationManager} from 'react-notifications';
import userservice from "../../services/User/serviceUser";

const ListUser = () => {
    const [DetailData, setDetailData] = useState([]);
    const [loading, setLoading] = useState(false);
    
    const initialFormData = {
      oldPassword: "",
      newPassword: "",
      noKta : "",
      email : "",
      role : "",
      name : "",
      phoneNumber : ""
    };
    const [FormData, setFormData] = useState(initialFormData);
    
    const handleInputChange = event => {
      const { name, value } = event.target;
      setFormData({ ...FormData, [name]: value });
    };

    const required = (type, message) => {
      switch (type) {
        case 'info':
          NotificationManager.info(message, 'Info', 3000);
          break;
        case 'success':
          NotificationManager.success(message, 'Info', 3000);
          break;
        case 'warning':
          NotificationManager.warning(message, 'Info', 3000);
          break;
        case 'error':
          NotificationManager.error(message, 'Info', 3000);
          break;
      }
    };
    
    const retrieveData = () => {
      UserService.getAll(token)
        .then((response) => {
          setlistData(response.data.data);
        })
        .catch((e) => {
          console.log(e);
        });
    };

    const handleDetailData = (item) => {
      setDetailData(item);
      setLoading(false);
    };

    //delete data
    const handleDeleteData = (id) => {
      userservice.remove(id, token)
      .then(response => {
      setLoading(true);
      retrieveData()
        required('info', 'Data Berhasil Dihapus')
      })
      .catch(e => {
        console.log(e);
      });
    };
    
    //saveeditpassword
    const handleSavePasswordData = (id) => {
      
      var data = {
          newPassword: FormData.newPassword,
          oldPassword: FormData.oldPassword,
      };
      console.log(data)
      userservice.changePassword(id, data, token)
      .then(response => {
        if(response.data.statusCode >= '400'){
          required('warning', response.data.message)
        }else{
          setLoading(true);
          retrieveData()
          required('info', 'Password Berhasil diganti.')
          setFormData(initialFormData);
        }
      })
      .catch(e => {
        console.log(e);
      });
    };

    //saveedituser
    const handleSaveEditData = (id) => {
      
      var data = {
          noKta : FormData.noKta ? FormData.noKta : DetailData.noKta,
          email : FormData.email ? FormData.email : DetailData.email,
          role : FormData.role ? FormData.role : DetailData.role,
          name : FormData.name ? FormData.name : DetailData.name,
          phoneNumber : FormData.phoneNumber ? FormData.phoneNumber : DetailData.phoneNumber,
      };
      
      userservice.changeUser(id, data, token)
      .then(response => {
        if(response.data.statusCode >= '400'){
          required('warning', response.data.message)
        }else{
          setLoading(true);
          retrieveData()
          required('info', 'Edit User Berhasil.')
          setFormData(initialFormData);
        }
      })
      .catch(e => {
        console.log(e);
      });
    };

    //saveeditProfile
    const handleSaveEditProfile = (id) => {
      
      var data = {
          noKta : FormData.noKta ? FormData.noKta : DetailData.noKta,
          email : FormData.email ? FormData.email : DetailData.email,
          name : FormData.name ? FormData.name : DetailData.name,
          phoneNumber : FormData.phoneNumber ? FormData.phoneNumber : DetailData.phoneNumber,
      };
      
      userservice.changeUserProfile(id, data, token)
      .then(response => {
        if(response.data.statusCode >= '400'){
          required('warning', response.data.message)
        }else{
          setLoading(true);
          retrieveData()
          required('info', 'Edit User Berhasil.')
          setFormData(initialFormData);
        }
      })
      .catch(e => {
        console.log(e);
      });
    };
    
    const [listData, setlistData] = useState([]);
    const listDataRef = useRef();
    
    listDataRef.current = listData;
  
    const { user: currentUser } = useSelector((state) => state.auth);
    const token = localStorage.getItem('token')

    useEffect(() => {
      retrieveData();
    }, []);
                   
    if (!currentUser) {
        return <Redirect to="/login" />;
    }
         
    const columns = [
      {
        label: 'No',
        name: 'no',
      },
      {
        label: 'Nama',
        name: 'nama',
      },
      {
        label: 'No. KTA',
        name: 'noKta',
      },
      {
        label: 'email',
        name: 'email',
      },
      {
        label: 'No. Handphone',
        name: 'phoneNumber',
      },
      {
        label: 'Status',
        name: 'isActive',
      },
      {
        label: 'Role',
        name: 'role',
      },
      {
        label: 'Aksi',
        name: 'action',
        options: {
          filter: false,
          sort: false,
        },
      },
    ]
  
    const getdatatable = (listData) => {
      const newArray = []
      listData.forEach((item, i) => {
        const arrobj = {
          no: i + 1 ,
          nama: item.name,
          noKta: item.noKta,
          email: item.email,
          phoneNumber: item.phoneNumber,
          isActive: item.isActive === true ? 'Aktif' : 'Non Aktif',
          role: item.role,
          action: (
            <React.Fragment>
              {/* eslint no-underscore-dangle: 0 */}
              <button type="button" className="btn btn-primary mr-1" title="Edit Data" data-toggle="modal" data-target="#EditUserModal" onClick={() => handleDetailData(item)}>
                <FontAwesomeIcon icon={faEdit} />
              </button> 
              <button type="button" className="btn btn-secondary mr-1" title="Edit Profile" data-toggle="modal" data-target="#EditProfileModal" onClick={() => handleDetailData(item)}>
                <FontAwesomeIcon icon={faUserEdit} />
              </button> 
              <button type="button" className="btn btn-success mr-1" title="Reset Password" data-toggle="modal" data-target="#EditPasswordModal" onClick={() => handleDetailData(item)}>
                <FontAwesomeIcon icon={faUnlockAlt} />
              </button> 
              <button type="button" className="btn btn-danger mr-1" title="Delete Data" data-toggle="modal" data-target="#DeleteModal" onClick={() => handleDetailData(item)}>
                <FontAwesomeIcon icon={faTrashAlt} />
              </button>
            </React.Fragment>
          ),
        }
        newArray.push(arrobj)
      })
      return newArray;
    }
  
    const dataTable = getdatatable(listData)
    
    const options = {
      filterType: 'dropdown',
      responsive: 'stacked',
      filter: true,
      download: false,
      print: false,
      viewColumns: false,
      rowsPerPage: 5,
      rowsPerPageOptions : [5,15,25,100],
      selectableRows: false,
      textLabels: {
        body: {
          noMatch: 'Tidak Ada Data',
          toolTip: 'Sort',
          columnHeaderTooltip: column => `Sorting ${column.label}`,
        },
        pagination: {
          next: 'Selanjutnya',
          previous: 'Sebelumnya',
          rowsPerPage: 'Data per Halaman:',
          displayRows: 'dari',
        },
        toolbar: {
          search: 'Cari Data',
          downloadCsv: 'Download CSV',
          print: 'Print',
          viewColumns: 'Lihat Kolom',
          filterTable: 'Filter Tabel',
        },
        filter: {
          all: 'Semua',
          title: 'FILTERS',
          reset: 'RESET',
        },
        viewColumns: {
          title: 'Lihat Kolom',
          titleAria: 'Show/Hide Table Columns',
        },
        selectedRows: {
          text: 'row(s) selected',
          delete: 'Delete',
          deleteAria: 'Delete Selected Rows',
        },
      },
      customToolbar: () => {
        return (
          <React.Fragment>
            <a href="/user/tambah" type="button" className="btn btn-info">
              Tambah Data
            </a>
          </React.Fragment>
        );
      }
    }
    
    return (
    <div className="app-content content ">
      <NotificationContainer/>
      <div className="content-overlay"></div>
      <div className="header-navbar-shadow"></div>
      <div className="content-wrapper">
        <div className="content-header row"></div>
        <div className="content-body">
          <section id="dashboard">
            <div className="row match-height justify-content-md-center">
              <div className="col-lg-12 col-md-12 col-12 mt-3">
                <MUIDataTable
                    title="Data User"
                    data={dataTable}
                    columns={columns}
                    options={options}
                />
              </div>
            </div>
          </section>
        </div>
      </div>
      {/* modal Delete */}
      <div className="modal fade" id="DeleteModal" tabIndex="-1" role="dialog" aria-labelledby="DeleteModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="DeleteModalLabel">Detail Data</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <div className="row">
                <div className="col-sm-12">
                  <div className="card-body">
                  <div className="form-group">
                      <div className="row">
                          <div className="col-md-3">
                              <label htmlFor="name">Nama</label>
                          </div>
                          <div className="col-md-9">
                            <input type="text" className="form-control" defaultValue={DetailData.name} readOnly="readOnly"/>
                          </div>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" className="btn btn-primary" disabled={loading} onClick={() => handleDeleteData(DetailData?._id)}>Hapus</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    
      {/* modal EditPassword */}
      <div className="modal fade" id="EditPasswordModal" tabIndex="-1" role="dialog" aria-labelledby="EditPasswordModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="EditPasswordModalLabel">Edit Password</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <div className="row">
                <div className="col-sm-12">
                  <div className="card-body">
                    <div className="form-group">
                      <div className="row">
                        <div className="col-md-4">
                            <label htmlFor="passwordlama">Password Lama</label>
                        </div>
                        <div className="col-md-8">
                          <input 
                            type="password"
                            className="form-control"
                            id="oldPassword"
                            value={FormData.oldPassword}
                            onChange={handleInputChange}
                            name="oldPassword"
                            placeholder="********"/>
                        </div>
                      </div>
                    </div>
                    <div className="form-group">
                      <div className="row">
                        <div className="col-md-4">
                            <label htmlFor="passwordbaru">Password Baru</label>
                        </div>
                        <div className="col-md-8">
                          <input 
                            type="password"
                            className="form-control"
                            id="newPassword"
                            value={FormData.newPassword}
                            onChange={handleInputChange}
                            name="newPassword"
                            placeholder="********"/>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" className="btn btn-primary" disabled={loading} onClick={() => handleSavePasswordData(DetailData?._id)}>Simpan</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    
      {/* modal EditUser */}
      <div className="modal fade" id="EditUserModal" tabIndex="-1" role="dialog" aria-labelledby="EditUserModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="EditUserModalLabel">Edit User</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <div className="row">
                <div className="col-md-12">
                  <div className="form-group">
                      <div className="row">
                          <div className="col-md-4">
                              <label htmlFor="noKta">No. KTA</label>
                          </div>
                          <div className="col-md-8">
                              <input
                                  type="text"
                                  className="form-control"
                                  id="noKta"
                                  defaultValue={DetailData.noKta}
                                  onChange={handleInputChange}
                                  name="noKta"
                              />
                          </div>
                      </div>
                  </div>
                  <div className="form-group">
                      <div className="row">
                          <div className="col-md-4">
                              <label htmlFor="name">Nama</label>
                          </div>
                          <div className="col-md-8">
                              <input
                                  type="text"
                                  className="form-control"
                                  id="name"
                                  defaultValue={DetailData.name}
                                  onChange={handleInputChange}
                                  name="name"
                              />
                          </div>
                      </div>
                  </div>
                  <div className="form-group">
                      <div className="row">
                          <div className="col-md-4">
                              <label htmlFor="phoneNumber">No. Handphone</label>
                          </div>
                          <div className="col-md-8">
                              <input
                                  type="text"
                                  className="form-control"
                                  id="phoneNumber"
                                  defaultValue={DetailData.phoneNumber}
                                  onChange={handleInputChange}
                                  name="phoneNumber"
                              />
                          </div>
                      </div>
                  </div>
                  <div className="form-group">
                      <div className="row">
                          <div className="col-md-4">
                              <label htmlFor="email">Email</label>
                          </div>
                          <div className="col-md-8">
                              <input
                                  type="email"
                                  className="form-control"
                                  id="email"
                                  defaultValue={DetailData.email}
                                  onChange={handleInputChange}
                                  name="email"
                              />
                          </div>
                      </div>
                  </div>
                  <div className="form-group">
                      <div className="row">
                          <div className="col-md-4">
                              <label htmlFor="role">Role</label>
                          </div>
                          <div className="col-md-8">
                              <select 
                                  type="role"
                                  className="form-control"
                                  id="role"
                                  onChange={handleInputChange}
                                  name="role"
                              >
                                  <option value="">- Pilih Role -</option>
                                  <option value="ppns"  selected={DetailData.role === 'ppns' ? 'selected':""}>PPNS</option>
                                  <option value="admin" selected={DetailData.role === 'admin' ? 'selected':""}>Admin</option>
                              </select>
                          </div>
                      </div>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" className="btn btn-primary" disabled={loading} onClick={() => handleSaveEditData(DetailData?._id)}>Simpan</button>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* modal EditProfile */}
      <div className="modal fade" id="EditProfileModal" tabIndex="-1" role="dialog" aria-labelledby="EditProfileModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="EditProfileModalLabel">Edit User</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <div className="row">
                <div className="col-md-12">
                  <div className="form-group">
                      <div className="row">
                          <div className="col-md-4">
                              <label htmlFor="noKta">No. KTA</label>
                          </div>
                          <div className="col-md-8">
                              <input
                                  type="text"
                                  className="form-control"
                                  id="noKta"
                                  defaultValue={DetailData.noKta}
                                  onChange={handleInputChange}
                                  name="noKta"
                              />
                          </div>
                      </div>
                  </div>
                  <div className="form-group">
                      <div className="row">
                          <div className="col-md-4">
                              <label htmlFor="name">Nama</label>
                          </div>
                          <div className="col-md-8">
                              <input
                                  type="text"
                                  className="form-control"
                                  id="name"
                                  defaultValue={DetailData.name}
                                  onChange={handleInputChange}
                                  name="name"
                              />
                          </div>
                      </div>
                  </div>
                  <div className="form-group">
                      <div className="row">
                          <div className="col-md-4">
                              <label htmlFor="phoneNumber">No. Handphone</label>
                          </div>
                          <div className="col-md-8">
                              <input
                                  type="text"
                                  className="form-control"
                                  id="phoneNumber"
                                  defaultValue={DetailData.phoneNumber}
                                  onChange={handleInputChange}
                                  name="phoneNumber"
                              />
                          </div>
                      </div>
                  </div>
                  <div className="form-group">
                      <div className="row">
                          <div className="col-md-4">
                              <label htmlFor="email">Email</label>
                          </div>
                          <div className="col-md-8">
                              <input
                                  type="email"
                                  className="form-control"
                                  id="email"
                                  defaultValue={DetailData.email}
                                  onChange={handleInputChange}
                                  name="email"
                              />
                          </div>
                      </div>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" className="btn btn-primary" disabled={loading} onClick={() => handleSaveEditProfile(DetailData?._id)}>Simpan</button>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  );
};

export default ListUser;
