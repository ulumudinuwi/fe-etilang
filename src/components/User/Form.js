import React, { useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Redirect } from 'react-router-dom';

import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';

import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";

import { login , me} from "../../actions/users/action";
import userservice from "../../services/User/serviceUser";


const Forms = (props) => {
  const form = useRef();
  const checkBtn = useRef();
  const token = localStorage.getItem('token')
  
  const required = (type, message) => {
    switch (type) {
      case 'info':
        NotificationManager.info(message, 'Info', 3000);
        break;
      case 'success':
        NotificationManager.success(message, 'Info', 3000);
        break;
      case 'warning':
        NotificationManager.warning(message, 'Info', 3000);
        break;
      case 'error':
        NotificationManager.error(message, 'Info', 3000);
        break;
    }
  };

  const initialFormDataState = {
    noKta: "",
    email: "",
    role: "",
    password: "",
    name: "",
    phoneNumber: "",
  };

  const [FormData, setFormData] = useState(initialFormDataState);
  
  const handleInputChange = event => {
    const { name, value } = event.target;
    setFormData({ ...FormData, [name]: value });
  };

  const [loading, setLoading] = useState(false);

  const saveFormData = (e) => {
    e.preventDefault();

    setLoading(true);

    var data = {
        noKta: FormData.noKta,
        email: FormData.email,
        role: FormData.role,
        password: FormData.password,
        name: FormData.name,
        phoneNumber: FormData.phoneNumber,
    };

    userservice.create(data, token)
      .then(response => {
        setLoading(false);
        console.log(response.data);
        if(response.data.statusCode == '400'){
            response.data.message.forEach(element => {
                required('warning', element)
            });
        }else if(response.data.statusCode == '409'){
            required('warning', response.data.message)
        }else{
            required('info', response.data.message)
            setTimeout(() => {            
                props.history.push("/user/list");
                newFormData()
            }, 2000);
        }
      })
      .catch(e => {
        console.log(e);
      });
  };
  
  const newFormData = () => {
    setFormData(initialFormDataState);
  };

  const { user: currentUser } = useSelector((state) => state.auth);

  if (!currentUser) {
    return <Redirect to="/login" />;
  }
  return (
    
    <div className="app-content content ">
      <div className="content-overlay"></div>
      <div className="header-navbar-shadow"></div>
      <div className="content-wrapper">
        <div className="content-header row"></div>
        <div className="content-body">
            <div className="card mb-0">
                <div className="card-body">
                    <NotificationContainer/>
                        <Form onSubmit={saveFormData} ref={form}>
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <div className="row">
                                            <div className="col-md-3">
                                                <label htmlFor="noKta">No. KTA</label>
                                            </div>
                                            <div className="col-md-9">
                                                <Input
                                                    type="text"
                                                    className="form-control"
                                                    id="noKta"
                                                    value={FormData.noKta}
                                                    onChange={handleInputChange}
                                                    name="noKta"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="row">
                                            <div className="col-md-3">
                                                <label htmlFor="name">Nama</label>
                                            </div>
                                            <div className="col-md-9">
                                                <Input
                                                    type="text"
                                                    className="form-control"
                                                    id="name"
                                                    value={FormData.name}
                                                    onChange={handleInputChange}
                                                    name="name"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="row">
                                            <div className="col-md-3">
                                                <label htmlFor="phoneNumber">No. Handphone</label>
                                            </div>
                                            <div className="col-md-9">
                                                <Input
                                                    type="text"
                                                    className="form-control"
                                                    id="phoneNumber"
                                                    value={FormData.phoneNumber}
                                                    onChange={handleInputChange}
                                                    name="phoneNumber"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="row">
                                            <div className="col-md-3">
                                                <label htmlFor="email">Email</label>
                                            </div>
                                            <div className="col-md-9">
                                                <Input
                                                    type="email"
                                                    className="form-control"
                                                    id="email"
                                                    value={FormData.email}
                                                    onChange={handleInputChange}
                                                    name="email"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="row">
                                            <div className="col-md-3">
                                                <label htmlFor="password">Password</label>
                                            </div>
                                            <div className="col-md-9">
                                                <Input
                                                    type="password"
                                                    className="form-control"
                                                    id="password"
                                                    value={FormData.password}
                                                    onChange={handleInputChange}
                                                    name="password"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="row">
                                            <div className="col-md-3">
                                                <label htmlFor="role">Role</label>
                                            </div>
                                            <div className="col-md-9">
                                                <select 
                                                    type="role"
                                                    className="form-control"
                                                    id="role"
                                                    value={FormData.role}
                                                    onChange={handleInputChange}
                                                    name="role"
                                                >
                                                    <option value="">- Pilih Role -</option>
                                                    <option value="admin">Admin</option>
                                                    <option value="ppns">PPNS</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div className="form-group">
                                <button className="btn btn-primary btn-block" disabled={loading}>
                                {loading && (
                                    <span className="spinner-border spinner-border-sm"></span>
                                )}
                                <span>Simpan</span>
                                </button>
                                <a href="/user/list" className="btn btn-warning btn-block">
                                    <span>Batal</span>
                                </a>
                            </div>
                        </div>
                    </Form>
                </div>
            </div>
        </div>
    </div>
  </div>
  );
};

export default Forms;
