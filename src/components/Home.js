import React from "react";
import { Redirect } from 'react-router-dom';
import { useSelector } from "react-redux";

const Home = () => {
  const { user: currentUser } = useSelector((state) => state.auth);

  if (!currentUser) {
    return <Redirect to="/login" />;
  }

  return (
    <div className="app-content content ">
      <div className="content-overlay"></div>
      <div className="header-navbar-shadow"></div>
      <div className="content-wrapper">
        <div className="content-header row"></div>
        <div className="content-body">
          <section id="dashboard">
            <div className="row match-height justify-content-md-center">
              <div className="col-lg-6 col-md-6 col-12 ">
                <div className="card card-developer-meetup">
                    <div className="meetup-img-wrapper rounded-top text-center">
                        <img src="app-assets/images/hubdat-assets/vector-1.svg" alt="plokis" height="170" />
                    </div>
                    <div className="card-body">
                        <div className="meetup-header d-flex align-items-center">
                            <div className="meetup-day">
                                <h6 className="mb-0">Selasa</h6>
                                <h3 className="mb-0">24</h3>
                            </div>
                            <div className="my-auto">
                                <h4 className="card-title mb-25">Dirjen Perhubungan Darat</h4>
                                <p className="card-text mb-0">Perkara Tialng Untuk Memenuhi Kinerja</p>
                            </div>
                        </div>
                        <div className="media">
                            <div className="avatar bg-light-primary rounded mr-1">
                                <div className="avatar-content">
                                    <svg id="Capa_1"  height="500" viewBox=" 0 0   500 500" width="500" xmlns="http://www.w3.org/2000/svg"><g><path d="m281.941 166.464h46.067c5.523 0 10-4.477 10-10v-46.144c0-5.523-4.477-10-10-10h-46.067c-5.523 0-10 4.477-10 10v46.144c0 5.523 4.477 10 10 10zm10-46.143h26.067v26.144h-26.067z"/><path d="m191.212 166.464h46.067c5.523 0 10-4.477 10-10v-46.144c0-5.523-4.477-10-10-10h-46.067c-5.523 0-10 4.477-10 10v46.144c0 5.523 4.477 10 10 10zm10-46.143h26.067v26.144h-26.067z"/><path d="m281.941 268.043h46.067c5.523 0 10-4.477 10-10v-46.143c0-5.523-4.477-10-10-10h-46.067c-5.523 0-10 4.477-10 10v46.143c0 5.522 4.477 10 10 10zm10-46.144h26.067v26.143h-26.067z"/><path d="m191.212 268.043h46.067c5.523 0 10-4.477 10-10v-46.143c0-5.523-4.477-10-10-10h-46.067c-5.523 0-10 4.477-10 10v46.143c0 5.522 4.477 10 10 10zm10-46.144h26.067v26.143h-26.067z"/><path d="m281.941 369.621h46.067c5.523 0 10-4.477 10-10v-46.144c0-5.523-4.477-10-10-10h-46.067c-5.523 0-10 4.477-10 10v46.144c0 5.523 4.477 10 10 10zm10-46.143h26.067v26.144h-26.067z"/><path d="m237.279 369.621c5.523 0 10-4.477 10-10v-46.144c0-5.523-4.477-10-10-10h-46.067c-5.523 0-10 4.477-10 10v46.144c0 5.523 4.477 10 10 10zm-36.067-46.143h26.067v26.144h-26.067z"/><path d="m463.95 492h-9.298v-302.052c0-5.523-4.477-10-10-10h-65.152v-123.991c0-5.523-4.477-10-10-10h-21.306v-35.957c0-5.523-4.477-10-10-10h-157.168c-5.523 0-10 4.477-10 10v35.957h-21.305c-5.523 0-10 4.477-10 10v192.461h-54.868c-5.523 0-10 4.477-10 10v233.582h-26.803c-5.523 0-10 4.477-10 10s4.477 10 10 10h415.9c5.523 0 10-4.477 10-10s-4.477-10-10-10zm-272.924-472h137.167v25.957h-137.167zm-96.173 248.418h44.868v223.582h-44.868zm64.868-202.461h199.779v426.043h-49.78v-65.978c0-5.523-4.477-10-10-10h-80.22c-5.523 0-10 4.477-10 10v65.978h-49.779zm69.78 426.043v-55.978h60.22v55.978zm149.999 0v-292.052h55.152v292.052z"/></g></svg>
                                </div>
                            </div>
                            <div className="media-body">
                                <h6 className="mb-0">UPPKB Losarang</h6>
                                <small>Unit Pelaksana Penimbangan Kendaraan Bermotor</small>
                            </div>
                        </div>
                        <div className="media mt-2">
                            <div className="avatar bg-light-primary rounded mr-1">
                                <div className="avatar-content">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-map-pin avatar-icon font-medium-3"><path d="M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z"></path><circle cx="12" cy="10" r="3"></circle></svg>
                                </div>
                            </div>
                            <div className="media-body">
                                <h6 className="mb-0">JABAR</h6>
                                <small>Jawa Barat, Bandung Utara</small>
                            </div>
                        </div>
                        
                    </div>
                </div>
              </div>
            </div>
          </section>
          <section id="perkara">
            <div className="row match-height  ">
              <div className="col-lg-4 col-md-6 col-12 ">
                <div className="card   card-for-all">
                  <h4 className="card-header bg-primary text-white">Jadwal Persidangan H+7</h4>
                  <div className="for-table">
                  <table className="table-custome">
                    <thead>
                      <tr>
                        <th rowSpan="2" className="text-center">Tanggal</th>
                        <th colSpan="5" className="text-center">Jumlah Perkara</th>
                    
                      </tr>
                      <tr>
                        <th className="text-center">Merah</th> 
                        <th>Biru</th> 
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td > <strong>20 Maret 2020 </strong> </td>
                        <td>200</td>
                        <td>100</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                </div>
              </div>
              <div className="col-lg-4 col-md-6 col-12 ">
                <div className="card   card-for-all">
                  <h4 className="card-header bg-primary text-white">Jadwal Persidangan H+7</h4>
                  <div className="for-table">
                  <table className="table-custome">
                    <thead>
                      <tr>
                        <th rowSpan="2" className="text-center">Tanggal</th>
                        <th colSpan="5" className="text-center">Jumlah Perkara</th>
                    
                      </tr>
                      <tr>
                        <th className="text-center">Merah</th> 
                        <th>Biru</th> 
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td > <strong>20 Maret 2020 </strong> </td>
                        <td>200</td>
                        <td>100</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                </div>
              </div>
              <div className="col-lg-4 col-md-6 col-12 ">
                <div className="card   card-for-all">
                  <h4 className="card-header bg-primary text-white">Jadwal Persidangan H+7</h4>
                  <div className="for-table">
                  <table className="table-custome">
                    <thead>
                      <tr>
                        <th rowSpan="2" className="text-center">Tanggal</th>
                        <th colSpan="5" className="text-center">Jumlah Perkara</th>
                    
                      </tr>
                      <tr>
                        <th className="text-center">Merah</th> 
                        <th>Biru</th> 
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td > <strong>20 Maret 2020 </strong> </td>
                        <td>200</td>
                        <td>100</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                </div>
              </div>
              
  
            </div>
          </section>
  
        </div>
      </div>
    </div>
  );
};

export default Home;
