import React, { useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Redirect } from 'react-router-dom';

import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';

import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";

import { login , me} from "../actions/auth";
import userservice from "../services/User/serviceUser";

const Login = (props) => {
  const form = useRef();
  const checkBtn = useRef();

  const required = (type, message) => {
    switch (type) {
      case 'info':
        NotificationManager.info(message, 'Info', 3000);
        break;
      case 'success':
        NotificationManager.success(message, 'Info', 3000);
        break;
      case 'warning':
        NotificationManager.warning(message, 'Info', 3000);
        break;
      case 'error':
        NotificationManager.error(message, 'Info', 3000);
        break;
    }
  };

  const initialFormData = {
    email : "",
    activationCode : "",
    newPassword : "",
    forgotPasswordCode : "",
  };
  const [FormData, setFormData] = useState(initialFormData);
  
  const handleInputChange = event => {
    const { name, value } = event.target;
    setFormData({ ...FormData, [name]: value });
  };

  //Aktivasi
  const AktivasiHandle = (e) => {
    e.preventDefault();
    setLoading(true);

    var data = {
      email: FormData.email,
      activationCode: FormData.activationCode,
    };

    userservice.AktivasiUser(data)
    .then(response => {
      if(response.data.statusCode >= '400'){
        setLoading(false);
        required('warning', response.data.message)
      }else{
        setLoading(false);
        required('info', response.data.message)
        setFormData(initialFormData);
      }
    })
    .catch(e => {
      console.log(e);
    });
  };

  //Lupa Password
  const LupaPasswordHandle = (e) => {
    e.preventDefault();
    setLoading(true);

    userservice.LupaPasswordUser(FormData.email)
    .then(response => {
      if(response.data.statusCode >= '400'){
        setLoading(false);
        required('warning', response.data.message)
      }else{
        setLoading(false);
        required('info', response.data.message)
        setFormData(initialFormData);
      }
    })
    .catch(e => {
      console.log(e);
    });
  };
  
  //Reset
  const ResetHandle = (e) => {
    e.preventDefault();
    setLoading(true);

    var data = {
      email: FormData.email,
      forgotPasswordCode: FormData.forgotPasswordCode,
      newPassword: FormData.newPassword,
    };

    userservice.ResetUser(data)
    .then(response => {
      if(response.data.statusCode >= '400'){
        setLoading(false);
          required('warning', response.data.message)
          response.data.message.forEach(element => {
              required('warning', element)
          });
      }else{
        setLoading(false);
        required('info', response.data.message)
        setFormData(initialFormData);
      }
    })
    .catch(e => {
      console.log(e);
    });
  };
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);

  const { isLoggedIn } = useSelector(state => state.auth);
  const { message } = useSelector(state => state.message);

  const dispatch = useDispatch();

  const onChangeUsername = (e) => {
    const username = e.target.value;
    setUsername(username);
  };

  const onChangePassword = (e) => {
    const password = e.target.value;
    setPassword(password);
  };

  const handleLogin = (e) => {
    e.preventDefault();

    setLoading(true);

    form.current.validateAll();

    if(!username){
      setLoading(false);
      return required('warning', 'NRP Harus di isi')
    }

    if(!password){
      setLoading(false);
      return required('warning', 'Password Harus di isi')
    }

    if (checkBtn.current.context._errors.length === 0) {
      dispatch(login(username, password))
        .then((response) => {
          dispatch(me(response.data.access_token))
            .then(() => {
              props.history.push("/dashboard");
              window.location.reload();
              return required('info', response.data.message)
            })
            .catch((response) => {
              setLoading(false);
              return required('warning', 'Invalid credentials')
            });
        })
        .catch((response) => {
          setLoading(false);
          return required('warning', 'Invalid credentials')
        });
    } else {
      setLoading(false);
    }
  };

  if (isLoggedIn) {
    return <Redirect to="/dashboard" />;
  }

  return (
    
    <div className="auth-wrapper auth-v1 px-2">
    <div className="auth-inner py-2">
      <div className="card mb-0">
        <div className="card-body">
          <a href="#" className="brand-logo">
            <img src="app-assets/images/hubdat-assets/logo-login.png" alt=""/>
          </a>
          <NotificationContainer/>

          <Form onSubmit={handleLogin} ref={form}>
          <div className="form-group">
            <label htmlFor="username">Login/NRP</label>
            <Input
              type="text"
              className="form-control"
              name="username"
              placeholder="NRP"
              value={username}
              onChange={onChangeUsername}
            />
          </div>

          <div className="form-group" >
            <label htmlFor="password">Password</label>
            <div className="input-group input-group-merge form-password-toggle">
              <input
                type="password"
                className="form-control"
                name="password"
                placeholder="******"
                value={password}
                onChange={onChangePassword}
              />
              <div className="input-group-append">
                <span className="input-group-text cursor-pointer"><i data-feather="eye"></i></span>
              </div>
            </div>
          </div>

          <div className="form-group">
            <button className="btn btn-primary btn-block" disabled={loading}>
              {loading && (
                <span className="spinner-border spinner-border-sm"></span>
              )}
              <span>Login</span>
            </button>
            <div className="row">
              <div className="col-md-12 mt-2">    
                  <button type="button" className="btn btn-outline-primary mr-1" title="Aktivasi User" data-toggle="modal" data-target="#AktivasiModal">
                    <span>Aktivasi !</span>
                  </button> 
                  <button type="button" className="btn btn-outline-info mr-1" title="Lupa Sandi ?" data-toggle="modal" data-target="#LupaSandiModal">
                    <span>Lupa Sandi ?</span>
                  </button> 
                  <button type="button" className="btn btn-outline-danger" title="Reset Sandi" data-toggle="modal" data-target="#ResetSandiModal">
                    <span>Reset Sandi</span>
                  </button> 
              </div>
            </div>
          </div>

          {message && (
            <div className="form-group">
              <div className="alert alert-danger" role="alert">
                {message}
              </div>
            </div>
          )}
          <CheckButton style={{ display: "none" }} ref={checkBtn} />
          </Form>
        </div>
      </div>
      
      {/* modal Aktivasi */}
      <div className="modal fade" id="AktivasiModal" tabIndex="-1" role="dialog" aria-labelledby="AktivasiModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="AktivasiModalLabel">Aktivasi Akun</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <div className="row">
                <div className="col-sm-12">
                  <div className="card-body">
                    <div className="form-group">
                      <div className="row">
                        <div className="col-md-4">
                            <label htmlFor="email">Email</label>
                        </div>
                        <div className="col-md-8">
                          <input 
                            type="email"
                            className="form-control"
                            id="email"
                            value={FormData.email}
                            onChange={handleInputChange}
                            name="email"/>
                        </div>
                      </div>
                    </div>
                    <div className="form-group">
                      <div className="row">
                        <div className="col-md-4">
                            <label htmlFor="activationCode">Kode Aktivasi</label>
                        </div>
                        <div className="col-md-8">
                          <input 
                            type="text"
                            className="form-control"
                            id="activationCode"
                            value={FormData.activationCode}
                            onChange={handleInputChange}
                            name="activationCode"/>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" className="btn btn-primary" disabled={loading} onClick={(e) => AktivasiHandle(e)}>Simpan</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    
      {/* modal Lupa Sandi */}
      <div className="modal fade" id="LupaSandiModal" tabIndex="-1" role="dialog" aria-labelledby="LupaSandiModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="LupaSandiModalLabel">Lupa Password</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <div className="row">
                <div className="col-sm-12">
                  <div className="card-body">
                    <div className="form-group">
                      <div className="row">
                        <div className="col-md-4">
                            <label htmlFor="email">Email</label>
                        </div>
                        <div className="col-md-8">
                          <input 
                            type="email"
                            className="form-control"
                            id="email"
                            value={FormData.email}
                            onChange={handleInputChange}
                            name="email"/>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" className="btn btn-primary" disabled={loading} onClick={(e) => LupaPasswordHandle(e)}>Simpan</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    
      {/* modal Reset Password */}
      <div className="modal fade" id="ResetSandiModal" tabIndex="-1" role="dialog" aria-labelledby="ResetSandiModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="ResetSandiModalLabel">Reset Password</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <div className="row">
                <div className="col-sm-12">
                  <div className="card-body">
                    <div className="form-group">
                      <div className="row">
                        <div className="col-md-4">
                            <label htmlFor="email">Email</label>
                        </div>
                        <div className="col-md-8">
                          <input 
                            type="email"
                            className="form-control"
                            id="email"
                            value={FormData.email}
                            onChange={handleInputChange}
                            name="email"/>
                        </div>
                      </div>
                    </div>
                    <div className="form-group">
                      <div className="row">
                        <div className="col-md-4">
                            <label htmlFor="forgotPasswordCode">Kode </label>
                        </div>
                        <div className="col-md-8">
                          <input 
                            type="text"
                            className="form-control"
                            id="forgotPasswordCode"
                            value={FormData.forgotPasswordCode}
                            onChange={handleInputChange}
                            name="forgotPasswordCode"/>
                        </div>
                      </div>
                    </div>
                    <div className="form-group">
                      <div className="row">
                        <div className="col-md-4">
                            <label htmlFor="newPassword">Password Baru</label>
                        </div>
                        <div className="col-md-8">
                          <input 
                            type="password"
                            className="form-control"
                            id="newPassword"
                            placeholder="*******"
                            value={FormData.newPassword}
                            onChange={handleInputChange}
                            name="newPassword"/>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" className="btn btn-primary" disabled={loading} onClick={(e) => ResetHandle(e)}>Simpan</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    
    </div>
  </div>
  );
};

export default Login;
