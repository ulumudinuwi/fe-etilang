import React, { useEffect, useState } from "react";
import { Redirect, useLocation  } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser, faHome, faListAlt, faArchive, faUsersCog, faPowerOff } from '@fortawesome/free-solid-svg-icons'

import { useDispatch, useSelector } from "react-redux";

import { logout } from "../actions/auth";

const Navbar = () => {
  const { user: currentUser } = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  const location = useLocation()
  const splitLocation = location.pathname.split("/");

  const [showModeratorBoard, setShowModeratorBoard] = useState(false);
  const [showAdminBoard, setShowAdminBoard] = useState(false);
  
  useEffect(() => {
    if (currentUser) {
      setShowModeratorBoard(currentUser.role.includes("ppns"));
      setShowAdminBoard(currentUser.role.includes("admin"));
    }
  }, [currentUser]);
  
  if (!currentUser) {
    return <Redirect to="/login" />;
  }
  const logOut = () => {
    dispatch(logout());
  };

  return (
    <div>
      <div className="loader-bg">
          <div className="loader-track">
              <div className="loader-fill"></div>
          </div>
      </div>
      <nav className="header-navbar navbar-expand-lg navbar navbar-fixed align-items-center navbar-shadow navbar-brand-center"
          data-nav="brand-center">
          <div className="navbar-header d-xl-block d-none">
              <ul className="nav navbar-nav">
                  <li className="nav-item"><a className="navbar-brand" href="#">
                  </a></li>
              </ul>
          </div>
          <div className="navbar-container d-flex content container-for-logos">
              <div className=" cont-logo-desktop d-flex align-items-center">
                  <ul className="nav navbar-nav d-xl-none">
                      <li className="nav-item"><a className="nav-link menu-toggle"><i className="ficon"
                              data-feather="menu"></i></a></li>
                  </ul>
                  <ul className="nav navbar-nav for-logo-desktop">
                      <li>
                          <a href="#">
                          <img src={splitLocation[0]+"/app-assets/images/hubdat-assets/logo.png"} alt=""/>
                          </a>
                      </li>
                  </ul>
              </div>
              <ul className="nav navbar-nav align-items-center ml-auto">
                  <li className="nav-item dropdown dropdown-user">
                      <a className="nav-link dropdown-toggle dropdown-user-link" id="dropdown-user"
                          data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <div className="user-nav d-sm-flex d-none">
                          <span className="user-name font-weight-bolder text-white">{currentUser.name}</span>
                          <span className="user-status text-white">{currentUser.email}</span>
                          </div>
                          <span className="avatar">
                          <img className="round" src={splitLocation[0]+"/app-assets//images/portrait/small/avatar-s-11.jpg"} alt="avatar" height="40"
                              width="40" />
                          <span className="avatar-status-online"></span>
                          </span>
                      </a>
                      <div className="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-user">
                          <a className="dropdown-item" href="#"><FontAwesomeIcon icon={faUser} /> Profile</a>
                          <a className="dropdown-item" href="/login" onClick={logOut}><FontAwesomeIcon icon={faPowerOff} /> Logout</a>
                      </div>
                  </li>
              </ul>
          </div>
      </nav>
      <div className="horizontal-menu-wrapper">
        <div className="header-navbar navbar-expand-sm navbar navbar-horizontal floating-nav navbar-light navbar-shadow menu-border"
          role="navigation">
          {/* role="navigation" data-menu="menu-wrapper" data-menu-type="floating-nav"> */}
          <div className="navbar-header">
            <ul className="nav navbar-nav flex-row">
              <li className="nav-item nav-toggle"><a className="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i
                    className="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i></a></li>
            </ul>
          </div>
          <div className="shadow-bottom"></div>
          <div className="navbar-container main-menu-content" data-menu="menu-container">
            <ul className="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">
              <li className="  nav-item text-primary active"><a className="  nav-link d-flex align-items-center"
                  href="/dashboard"><FontAwesomeIcon icon={faHome} /><span data-i18n="Beranda">Dashboard</span></a>
              </li>
              <li className="nav-item"><a className="  nav-link d-flex align-items-center" href="perkara-tilang.html">
                <FontAwesomeIcon icon={faListAlt} /><span data-i18n="Ticket case">Perkara tilang</span></a>
              </li>
              <li className="dropdown nav-item" data-menu="dropdown"><a className="dropdown-toggle nav-link d-flex align-items-center" href="#" data-toggle="dropdown"><FontAwesomeIcon icon={faArchive} /><span data-i18n="Report">Laporan</span></a>
                <ul className="dropdown-menu">
                  <li   data-menu=""><a className="dropdown-item d-flex align-items-center" href="#" data-toggle="dropdown" data-i18n="Analytics"><FontAwesomeIcon icon={faListAlt} /><span data-i18n="Report Case">Laporan Tilang</span></a>
                  </li>
                  <li data-menu=""><a className="dropdown-item d-flex align-items-center" href="#" data-toggle="dropdown" data-i18n="Laporan Pelanggaran"><FontAwesomeIcon icon={faListAlt} /><span data-i18n="Laporan Pelanggaran">Laporan Pelanggaran</span></a>
                  </li>
                </ul>
              </li>
              {showAdminBoard && (
              <li className="dropdown nav-item" data-menu="dropdown"><a className="dropdown-toggle nav-link d-flex align-items-center" href="#" data-toggle="dropdown"><FontAwesomeIcon icon={faUser} /><span data-i18n="Admin">Master Data</span></a>
                <ul className="dropdown-menu">
                  <li   data-menu=""><a className="dropdown-item d-flex align-items-center" href="/user/list" data-toggle="dropdown" data-i18n="Analytics"><FontAwesomeIcon icon={faUsersCog} /><span data-i18n="Report Case">Users</span></a>
                  </li>
                </ul>
              </li>
              )}
              <li className="dropdown nav-item" data-menu="dropdown"><a className="dropdown-toggle nav-link d-flex align-items-center" href="#" data-toggle="dropdown"><FontAwesomeIcon icon={faUser} /><span data-i18n="Admin">Admin</span></a>
                <ul className="dropdown-menu">
                  <li   data-menu="">
                    <a className="dropdown-item" href="#"><FontAwesomeIcon icon={faUser} /> Profile</a>
                  </li>
                  <li data-menu="">
                    <a className="dropdown-item" href="/login" onClick={logOut}><FontAwesomeIcon icon={faPowerOff} /> Logout</a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
      
    </div>
  );
};

export default Navbar;
