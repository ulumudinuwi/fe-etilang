import http from "../service";

const getAll = (token) => {
  return http.get("/users", token);
};

const get = (id) => {
  return http.get(`/users/${id}`);
};

const create = (data, token) => {
  return http.post("/auth/register", data, token);
};

const AktivasiUser = (data) => {
  return http.post("/auth/verify", data);
};

const ResetUser = (data) => {
  return http.post("/auth/reset-password", data);
};

const LupaPasswordUser = (data) => {
  return http.post(`/auth/forgot-password/${data}`);
};

const changePassword = (id, data, token) => {
  return http.put(`/users/${id}/password`, data, token);
};

const changeUser = (id, data, token) => {
  return http.put(`/users/${id}`, data, token);
};

const changeUserProfile = (id, data, token) => {
  return http.put(`/users/${id}/profile`, data, token);
};

const remove = (id,token) => {
  return http.delete(`/users/${id}`, token);
};

const findByTitle = (title) => {
  return http.get(`/users?title=${title}`);
};

const userservice = {
  getAll,
  get,
  create,
  changePassword,
  changeUser,
  changeUserProfile,
  findByTitle,
  remove,
  AktivasiUser,
  LupaPasswordUser,
  ResetUser,
};

export default userservice;
