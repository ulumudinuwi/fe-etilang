import service from './service'

const register = (username, email, password) => {
  return service.post("/signup", {
    username,
    email,
    password,
  });
};

const me = (token) => {
  return service
    .get("/auth/me", 'Bearer '+token)
    .then((response) => {
      if (response.data.data) {
        localStorage.setItem("user", JSON.stringify(response.data.data));
        localStorage.setItem('saved', new Date().getTime())
      }

      return response.data;
    });;
};

const login = (uniqueId, password) => {
  return service
    .post("/auth/login", {
      uniqueId,
      password,
    })
    .then((response) => {
      if (response.data.data) {
        localStorage.setItem("token", "Bearer "+response.data.data.access_token);
      }

      return response.data;
    });
};

const logout = () => {
  localStorage.clear();
  // localStorage.removeItem("user");
};

export default {
  register,
  login,
  me,
  logout,
};
