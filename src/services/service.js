import axios from 'axios'

const apiURL = process.env.REACT_APP_API_URL || 'http://128.199.194.80'
const headers = token => {
  const header = {
    'Content-Type': 'application/json',
  }

  if (token != null) header['Authorization'] = token
  return header
}

const headersdownload = token => {
  const headerss = {
    'Content-Disposition': 'attachment; filename=Teachers.xlsx',
    'Content-Type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    responseType: 'blob',
  }

  if (token != null) headerss['Authorization'] = token
  return headerss
}

const service = {
  post: (slug, body, token = null) =>
    axios.post(apiURL + slug, body, { headers: headers(token) }).catch(error => error.response),
  downloadpost: (slug, body, token = null) =>
    axios
      .post(apiURL + slug, body, { headers: headersdownload(token), responseType: 'arraybuffer' })
      .catch(error => error.response),
  put: (slug, body, token = null) =>
    axios.put(apiURL + slug, body, { headers: headers(token) }).catch(error => error.response),
  get: (slug, token = null, params = null) =>
    axios
      .get(apiURL + slug, {
        headers: headers(token),
        params,
      })
      .catch(error => error.response),
  download: (slug, token = null, params = null) =>
    axios
      .get(apiURL + slug, {
        headers: headersdownload(token),
        params,
        responseType: 'arraybuffer',
      })
      .catch(error => error.response),
  delete: (slug, token = null, params = null) =>
    axios
      .delete(apiURL + slug, {
        headers: headers(token),
        params,
      })
      .catch(error => error.response),
  deletepost: (slug, token = null, body = null) =>
    axios
      .delete(apiURL + slug, {
        headers: headers(token),
        data: body,
      })
      .catch(error => error.response),
}

export default service
