"use strict";
function filterColumn(a, e) {
    if (5 == a) {
        var t = $(".start_date").val(),
            l = $(".end_date").val();
        filterByDate(a, t, l), $(".dt-advanced-search").dataTable().fnDraw();
    } else $(".dt-advanced-search").DataTable().column(a).search(e, !1, !0).draw();
}
var separator = " - ",
    rangePickr = $(".flatpickr-range"),
    dateFormat = "MM/DD/YYYY",
    options = { autoUpdateInput: !1, autoApply: !0, locale: { format: dateFormat, separator: separator }, opens: "rtl" === $("html").attr("data-textdirection") ? "left" : "right" };
rangePickr.length &&
    rangePickr.flatpickr({
        mode: "range",
        dateFormat: "m/d/Y",
        onClose: function (a, e, t) {
            var l = "",
                n = new Date();
            null != a[0] && ((l = a[0].getMonth() + 1 + "/" + a[0].getDate() + "/" + a[0].getFullYear()), $(".start_date").val(l)),
                null != a[1] && ((n = a[1].getMonth() + 1 + "/" + a[1].getDate() + "/" + a[1].getFullYear()), $(".end_date").val(n)),
                $(rangePickr).trigger("change").trigger("keyup");
        },
    });
var filterByDate = function (a, e, t) {
        $.fn.dataTableExt.afnFiltering.push(function (l, n, s) {
            var o = normalizeDate(n[a]),
                r = normalizeDate(e),
                d = normalizeDate(t);
            return (r <= o && o <= d) || (o >= r && "" === d && "" !== r) || (o <= d && "" === r && "" !== d);
        });
    },
    normalizeDate = function (a) {
        var e = new Date(a);
        return e.getFullYear() + "" + ("0" + (e.getMonth() + 1)).slice(-2) + ("0" + e.getDate()).slice(-2);
    };
$(function () {
    $("html").attr("data-textdirection");
    var a = $(".datatables-ajax"),
        e = $(".dt-column-search"),
        t = $(".dt-advanced-search"),
        l = $(".dt-responsive"),
        n = "../../../hubdat/app-assets/";
    
   
    if (t.length)
        t.DataTable({
            ajax: n + "data/table-datatable.json",
            columns: [
                { data: "responsive_id" },
                { data: "id" },
                { data: "no_pembayaran" },
                { data: "date" },
                { data: "name" },
                { data: "penindak" },
                { data: "formulir" },
                { data: "uang" },
                { data: "denda" },
                { data: "tanggal-sidang" },
                { data: "tanggal-no" },
                { data: "res-id" }
                
                
            ],
            columnDefs: [{ className: "control", orderable: !0, targets: 0 },
            {
                targets: -1,
                title: "Actions",
                orderable: !1,
                render: function (e, a, t, s) {
                    return (
                        '<div class="d-inline-flex">'+'<a href="javascript:;" class="btn btn-primary btn-sm mr-1 font-small-2">Ganti Blanko</a><a href="javascript:;" class="item-edit btn btn-outline-primary font-small-2"> Edit </a>'
                    );
                },
            },],
            dom: '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            orderCellsTop: !0,
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (a) {
                            return "Details of " + a.data().full_name;
                        },
                    }),
                    type: "column",
                    renderer: $.fn.dataTable.Responsive.renderer.tableAll({ tableClass: "table" }),
                },
            },
            language: { paginate: { previous: "&nbsp;", next: "&nbsp;" } },
        });
    if (
        ($("input.dt-input").on("keyup", function () {
            filterColumn($(this).attr("data-column"), $(this).val());
        }),
        l.length)
    )
       
    $(".dataTables_filter .form-control").removeClass("form-control-sm"), $(".dataTables_length .custom-select").removeClass("custom-select-sm").removeClass("form-control-sm");
});
